Node export entity reference
----------------------------

This module can export a node with all entity references based on node import module.

Why would you use it?
---------------------

In those cases when you would like to move a few contents between different instances rather than using migration service of Drupal use this! :) It is wonderful! :D

Installation:
-------------
drush en node_export_entity_reference -y
OR
Go to admin/modules and enapbe this module.

Settings:
---------
There are no settings of this module.

Usage:
------

  => Export:
  ----------
  - Go to node page (ex: node/1) and click on "Export with entity refs" tab. This will automatically look for entity reference fields and export them as well into a text file.

  => Import:
  ----------
  - Go to admin/content and click on "Import with entity refs" tab. Upload you exported file and press "Import button".

Warnings, usage limits, troubleshooting:
---------------------------------------
- If you get an error on import, please be sure that you tried to import into FULLY the same content type as the original was.
- If you cannot see your entity references, please be sure that you exported a content what have any entity reference field.
- If you cannot see your entity references, please be sure that you exported entity references WITH NODES! (usersm taxonomy etc... NOT SUPPORTED!)

Enjoy!
